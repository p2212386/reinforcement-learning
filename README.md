# reinforcement learning

L'apprentissage par renforcement consiste, pour un agent autonome  à apprendre les actions à prendre, à partir d'expériences, de façon à optimiser une récompense quantitative au cours du temps. L'agent est plongé au sein d'un environnement et prend ses décisions en fonction de son état courant. En retour, l'environnement procure à l'agent une récompense, qui peut être positive ou négative. L'agent cherche, au travers d'expériences itérées, un comportement décisionnel (appelé stratégie ou politique, et qui est une fonction associant à l'état courant l'action à exécuter) optimal, en ce sens qu'il maximise la somme des récompenses au cours du temps. 
Dans ce projet on va implémenter un environnement  Cartpole-v1 du package gym. 

 premièrement une agent réalise des action aléatoire dans l'environnement pour les actions il y ‘a 2 actions possible soit de faire un mouvement à droit ou bien à gauche et pour l’état il y’ a 4 états (l’angle de la vélocité, l’angle de la barre, la vélocité de la carte et la position),le but principale est d'entraîner un algorithme DQN (deep Q learning) pour que à un état donnée et une action donnée l’algorithme peut savoir le chemin optimale (policy) qui maximise les récompenses cumulés (l'équation de Markov).

 donc l'idée principale c’est de renforcer l'algorithme a suivre l'équation de bellman, Afin de renforcer l’algorithme à suivre l’équation de Bellman on va imposer un erreur entre la prédiction et le cible(target network),Cette erreur va être rétro propagée avec backward pour faire la mise à jour des poids afin de choisir les meilleures actions dans chaque étape.
 
Pour le facteur d'atténuation on va choisir 0.99 car chaque récompense est moin important qu’à la précèdent, ensuite on va appliquer la méthode de epsilon greedy pour définir le pourcentage de l’exploitation et de l’exploration , au début le pourcentage d'exportation est beaucoup plus important que celle de l’exploitation  et lors des epochs , c’est pourcentages vont être inversés .


